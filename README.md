# Statcheck-app

Second version of the excel-search app
This repo contains all the code and logic related to the flask server

# 1. Set-up

- Clone the repositroy :
  ```
  git clone https://gitlab.inria.fr/cedar/statistics-crawler.git
  ```
- Create a virtual environnement, we use [virtualenv](https://virtualenv.pypa.io/en/latest/installation.html):

  ```
  cd statistics-crawler
  virtualenv .env
  ```

- Install the python dependencies:
  ```
  source .env/bin/activate
  pip install -r requirements.txt
  ```

# 2. Crawling

The code for crawling statistics websites can be found at [data/crawler/](https://gitlab.inria.fr/cedar/statcheck-app/-/tree/main/src/data/crawler). To get started, you can focus on :

- the generic crawler [stat.py](https://gitlab.inria.fr/cedar/statcheck-app/-/blob/main/src/data/crawler/spiders/stat.py#L177),
- the crawler script [crawl.py](https://gitlab.inria.fr/cedar/statcheck-app/-/blob/main/src/data/scripts/crawl.py#L135)

## Getting started

After following the instructions in the [README](https://gitlab.inria.fr/cedar/statcheck-app/-/tree/main) (Section 1. & 2.), checkout to your specific branch:

- You can create your on branch, using:

```
git checkout -b <mybranch>
git push --set-upstream origin <mybranch>
```

## Command Line

To start the experiments, use the command `python3 -m data.scripts.crawl` :

- You can get help about the command line arguments using :

  ```
  python3 -m scripts.crawl -h
  ```

- You can run multiple crawlers from a file (containing a list of links):

  ```
  python3 -m scripts.crawl --out-dir /PATH/TO/AN/OUT/DIR --crawler-type generic --file /PATH/TO/A/FILE/CONTAINING/LINKS
  ```

  The file should looks like :

  ```
  https://www.assemblee-nationale.fr/
  http://www.senat.fr/
  https://www.journal-officiel.gouv.fr/
  http://www.credoc.fr/
  https://www.cnis.fr/
  ....
  ```

- You can run a single crawler from an url :

  ```
  python3 -m scripts.crawl --out-dir /PATH/TO/AN/OUT/DIR --crawler-type generic --url http://www.senat.fr/
  ```

- the --max-page (-mp) argument can be used to limit the number of pages crawled :

  ```
 
  ```

## Output Files

The previous command will create a folder (in the path given using --out-dir) containing a maximun of 4 output files :

- html.link
- out_of_domain.link
- visited.link
- stat.tsv

### visited.link

This file contains the links to the webpages visited. Each line contains two links :

```
https://www.senat.fr/les-abonnements-du-senat.html	https://www.senat.fr/
```

The first is the page visited, the second is the page from which we obtained it. e.g we visited `https://www.senat.fr/les-abonnements-du-senat.html` from the page `https://www.senat.fr/`

### html.link

This file contains links to HTML tables (links where `<table>...</table>` tag has been found) :

```
https://www.assemblee-nationale.fr/presse/photos.asp#0.html
https://www.assemblee-nationale.fr/presse/photos.asp#1.html
https://www.assemblee-nationale.fr/presse/photos.asp#2.html
https://www.assemblee-nationale.fr/presse/photos.asp#3.html
```

e.g we found in the page `https://www.assemblee-nationale.fr/presse/photos.asp` 4 html tables

### out_of_domain.link

This file contains links to websites outside the domain being crawled :

Crawling `https://www.assemblee-nationale.fr/` we obtain :

```
https://www.acce-o.fr/client/assemblee-nationale
https://www.elections-legislatives.fr
https://www.parlementdesenfants.fr/
...
```

### stat.link

A file containing statistics about the websites crawled, for your experiments it will contain the start and the end date (in iso format) of the crawling:

```
start	2023-10-02T08:48:15.557983+00:00
end	2023-10-02T08:48:18.794480+00:00
```

## Analysis

In this step you should answer the questions written in the [notepad](https://notes.inria.fr/BIxqRxLORUmUNenrRYpytA#).
You can use the python tools of your choice. I recommend using [Jupyer Lab](https://jupyter.org/install#jupyterlab) for easy/quick development and [plotly](https://plotly.com/python/getting-started/) for visualisation.

**Important**: Keep all output folders/files, as we will retrieve them at the end of the project.

## Materials

- this wiki
- the issue [#90](https://gitlab.inria.fr/cedar/statcheck-app/-/issues/90), to be used for anything to do with the code(tag Ioana and myself).
- the [notepad](https://notes.inria.fr/BIxqRxLORUmUNenrRYpytA#), to be used for taking notes on meetings, observations, etc.
