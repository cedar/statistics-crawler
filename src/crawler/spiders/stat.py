import copy
import csv
import datetime
import re
import time
from pathlib import Path
from typing import AsyncIterable, Awaitable, Callable, Dict, Iterable, Sequence
from urllib.parse import unquote, urlparse

import requests
from scrapy import signals
from scrapy.exceptions import CloseSpider
from scrapy.http import HtmlResponse, Request, Response
from scrapy.link import Link
from scrapy.linkextractors import LinkExtractor
from scrapy.linkextractors.lxmlhtml import LxmlParserLinkExtractor
from scrapy.spiders import CrawlSpider, Rule, Spider
from scrapy.utils.asyncgen import collect_asyncgen
from scrapy.utils.spider import iterate_spider_output

from crawler.cache import UrlCache
from crawler.extractor import StatisticsLinkExtractor, StatisticsParserLinkExtractor
from utils.log import get_log

logger = get_log(__file__)


FILE_EXTENSIONS = [
    # archives
    "7z",
    "7zip",
    "bz2",
    "rar",
    "tar",
    "gz",
    "xz",
    "zip",
    # office suites
    "xlsx",
    "ods",
    "xlsm",
    "xls",
    # other
    "csv",
    "tsv",
    "pdf",
    "xml",
    "rdf",
    "rdfxml",
    "nt",
]

DENY_OO_DOMAINS = [
    "facebook",
    "twitter",
    "instagram",
    "linkedin",
    "tiktok",
    "youtube",
    "flickr",
]

EXCLUDED_KEYWORDS = [
    "mailto:",
    "rss.xml",
    "xml.htm",
]


class StatCrawlerMixin:
    def __init__(
        self,
        url: str,
        domain: str,
        should_be_prefixed: bool,
        out_dir: Path,
        max_page: int,
        override: bool = True,
    ) -> None:
        self.url = url
        self.domain = domain
        self.should_be_prefixed = should_be_prefixed
        self.out_dir = out_dir
        self.max_page = max_page
        self.visited_page = 0

        self.link_extractor = LxmlParserLinkExtractor()

        self.visited_file = None
        self.data_file = None
        self.html_file = None

        self.files_extensions = {f".{ext}" for ext in FILE_EXTENSIONS}
        self.deny_oo_domains = set(DENY_OO_DOMAINS)

        self._warmup_file(override)

    @staticmethod
    def _clean(url: str):
        return (
            url.replace("www.", "")
            .replace(".", "_")
            .replace(" ", "_")
            .replace("-", "_")
            .replace("/", "")
        )

    def _normalize_domain(self) -> str:
        """
        Normalize the domain to get a useful string for the output directory.
        Also handle the case where multiple subdomains are crawled concurrently by adding a prefix.
        """
        normalized_domain = self._clean(self.domain)

        if self.should_be_prefixed is not None:
            path = urlparse(self.url).path
            normalized_domain = f"{normalized_domain}_{self._clean(path)}"
        return normalized_domain

    def _warmup_file(self, override: bool):
        """
        Init the files to collect pages and items url.
        """
        normalize_domain = self._normalize_domain()
        self.normalize_domain = normalize_domain
        domain_dir = self.out_dir.joinpath(normalize_domain)

        domain_dir.mkdir(exist_ok=True, parents=True)

        self.visited_file = domain_dir.joinpath("visited.link")
        self.data_file = domain_dir.joinpath("data.link")
        self.html_file = domain_dir.joinpath("html.link")
        self.out_of_domain_file = domain_dir.joinpath("out_of_domain.link")
        self.stat_file = domain_dir.joinpath("stat.tsv")

        with self.visited_file.open("w") as f:
            writer = csv.DictWriter(f, delimiter="\t", fieldnames=["url", "from"])
            writer.writeheader()

        if override:
            if self.data_file.exists():
                self.data_file.unlink()

            if self.html_file.exists():
                self.html_file.unlink()

            if self.out_of_domain_file.exists():
                self.out_of_domain_file.unlink()

            if self.stat_file.exists():
                self.stat_file.unlink()

    def collect_visited_pages(self, current_url: str, from_url: str) -> None:
        """
        Collect the url of a visited page.
        """
        with self.visited_file.open("a") as f:
            writer = csv.DictWriter(f, delimiter="\t", fieldnames=["url", "from"])
            writer.writerow({"url": current_url, "from": from_url})

    def _save_html_file(self, html_table: HtmlResponse):
        raise NotImplementedError

    def collect_html_item(self, response: Response, download: bool) -> None:
        """
        Collect all html tables from a specific page.
        """
        html_tables = response.xpath("//table").getall()
        if html_tables:
            for idx, html_table in enumerate(html_tables):
                with self.html_file.open("a") as f:
                    if download:
                        self._save_html_file(html_table)
                    else:
                        f.write(f"{response.url}#{idx}.html\n")

    def _collect_downloadable_file(
        self, url: str, from_url: str, download: bool = False
    ) -> None:
        """
        Collect all downloadable links. e.g links containing targeted file extensions
        """
        for extension in self.files_extensions:
            if extension in url:
                with self.data_file.open("a") as f:
                    f.write(f"{url}\t{from_url}\n")
                break

    def _is_out_of_domain(self, url: str):
        """
        Check whether a link is out of the allowed domain.
        """
        seen_domain = urlparse(url).netloc
        if not seen_domain:
            return False

        allowed_domain = self.domain.lower()

        return not (
            (seen_domain == allowed_domain)
            or (seen_domain.endswith(f"{allowed_domain.replace('www', '')}"))
            or any(domain in seen_domain for domain in self.deny_oo_domains)
        )

    def _collect_out_of_domain_links(self, url: str) -> None:
        """
        Collect out of domains link url. Useful to see when and how statistics websites reference each other.
        """

        if self._is_out_of_domain(url):
            with self.out_of_domain_file.open("a") as f:
                f.write(f"{url}\n")

    def collect_datasets(self, response: Response, download: bool) -> None:
        """
        Collect all targeted links from a specific page.
        """

        logger.info(f"Content-Type: {response.headers.get('Content-Type')}")

        if "html" in response.headers.get("Content-Type", b"").decode("utf-8"):
            links = self.link_extractor.extract_links(response)

            # logger.info(f"links {links}")

            all_links = self.link_extractor._process_links(links)

            for link in all_links:
                self._collect_downloadable_file(link.url, response.url, download)
                self._collect_out_of_domain_links(link.url)

        else:
            content_disposition = response.headers.get("Content-Disposition").decode(
                "utf-8"
            )
            content_type = response.headers.get("Content-Type").decode("utf-8")
            try:
                if content_disposition:
                    filename = self._extract_filename_from_content_disposition(
                        content_disposition
                    )
                else:
                    filename = self._infer_filename_from_url(response.url, content_type)

                for extension in self.files_extensions:
                    if extension in filename:
                        with self.data_file.open("a") as f:
                            from_url = response.meta.get("from_url", response.url)
                            f.write(f"{response.url}#{filename}\t{from_url}\n")
                        break
            except Exception as e:
                logger.info(f"Error while extracting filename: {e}")

    def _infer_filename_from_url(self, url: str, content_type: str) -> str:
        """
        Infer the filename from the URL or content type.
        """
        parsed_url = urlparse(url)
        filename = unquote(parsed_url.path.split("/")[-1])

        return filename

    def _extract_filename_from_content_disposition(
        self, content_disposition: str
    ) -> str:
        """
        Extract filename from Content-Disposition header.
        """
        filename = re.findall('filename="?(.+?)"?$', content_disposition)
        if filename:
            return unquote(filename[0])
        return None

    def parse_item(self):
        """
        Function wrapper for close the spider if the number of visited page exceeed the number of max page to visit
        """
        self.visited_page += 1
        if self.max_page > 0 and self.visited_page > self.max_page:
            raise CloseSpider("Max Page exceeded")


class GenericCrawler(StatCrawlerMixin, CrawlSpider):
    name = "statistics_generic"
    handle_httpstatus_list = [500, 502, 403]

    def __init__(
        self,
        url: str,
        domain: str,
        out_dir: Path,
        max_page: int,
        download: bool = False,
        dev: bool = False,
        cache: UrlCache = None,
        should_be_prefixed: bool = False,
    ):
        super().__init__(
            url=url,
            domain=domain,
            should_be_prefixed=should_be_prefixed,
            out_dir=out_dir,
            max_page=max_page,
        )

        self.start_urls = [url]

        self.rules = [
            Rule(
                LinkExtractor(allow_domains=(domain), deny=[r"\/.*#.+"]),
                callback="parse_item",
                follow=True,
            )
        ]
        self._compile_rules()
        self.dev = dev

        self.download = download

        self.cache = cache

    def parse_item(self, response: Response):
        """
        Parse the items of the current page. And write out the page visited and the items into a file.
        """

        logger.info(f"Visiting {response.url}")
        super().parse_item()

        try:
            self.collect_html_item(response, self.download)

            self.collect_datasets(response, self.download)
        except Exception as e:
            logger.error(f"Error while parsing {response.url}: {e}")

    async def _parse_response(
        self,
        response: Response,
        callback: Callable,
        cb_kwargs: Dict,
        follow: bool = True,
    ):
        if self.dev:
            self.parse_item(response)
            if follow and self._follow_links:
                for request_or_item in self._requests_to_follow(response):
                    self.collect_visited_pages(
                        current_url=request_or_item.url,
                        from_url=response.url,
                    )
        else:
            if callback:
                cb_res = callback(response, **cb_kwargs) or ()
                if isinstance(cb_res, AsyncIterable):
                    cb_res = await collect_asyncgen(cb_res)
                elif isinstance(cb_res, Awaitable):
                    cb_res = await cb_res
                cb_res = self.process_results(response, cb_res)
                for request_or_item in iterate_spider_output(cb_res):
                    yield request_or_item

            if follow and self._follow_links:
                for request_or_item in self._requests_to_follow(response):
                    self.collect_visited_pages(
                        current_url=request_or_item.url,
                        from_url=response.url,
                    )
                    request_or_item.meta["from_url"] = response.url
                    yield request_or_item

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        spider = super(GenericCrawler, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_closed, signal=signals.spider_closed)
        return spider

    def spider_closed(self, spider, **kwargs):
        logger.info(
            f"Spider closed: {spider.name} for {self.url}, reason: {kwargs['reason']}",
        )
        start = self.crawler.stats.get_value("start_time")
        end = datetime.datetime.now(tz=datetime.timezone.utc)
        with self.stat_file.open("w") as f:
            f.write(f"start\t{start.isoformat()}\n")
            f.write(f"end\t{end.isoformat()}\n")


class FocusCrawler(StatCrawlerMixin, CrawlSpider):
    name = "statistics_focus"
    custom_settings = {
        "DEPTH": 0,
        "SCHEDULER_MEMORY_QUEUE": "data.crawler.scheduler.DoubleEndedMemoryQueue",
    }

    def __init__(self, url: str, domain: str, out_dir: Path, max_page: int):
        super().__init__(url=url, domain=domain, out_dir=out_dir, max_page=max_page)

        self.start_urls = [url]

        self.rules = [
            Rule(
                LinkExtractor(allow_domains=(domain), deny=[r"\/.*#.+"]),
                callback="parse_item",
                follow=True,
            )
        ]

        self._compile_rules()

    def parse_item(self, response: Response):
        """
        Parse the items of the current page. And write out the page visited and the items into a file.
        """
        logger.info(f"Visiting {response.url} ")
        super().parse_item()

        self.collect_visited_pages(response.url)
        self.collect_html_item(response)
        self.collect_datasets(response)
