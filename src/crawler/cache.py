import pickle as pkl
from pathlib import Path
from queue import Queue
from typing import Dict, List, Union

from utils.log import get_log

logger = get_log(__file__)


class Trie:
    end_token = "EOS"

    def __init__(self) -> None:
        self.node: Dict[str, Trie] = {}

    @staticmethod
    def _split_url(url: Union[str, List[str]]) -> Union[str, List[str]]:
        if isinstance(url, str):
            return url.split("/")
        return url

    def add(self, url: List[str]):
        """
        Insert part of the url in the trie
        """
        url = self._split_url(url)
        if len(url) == 0:
            self.node[self.end_token] = Trie()
        else:
            if url[0] not in self.node:
                self.node[url[0]] = Trie()

            self.node[url[0]].add(url[1:])

    def bfo_traverse(self) -> None:
        """
        Breath first order traversing, for debugging/analysing purpose
        """
        fifo_queue = Queue()
        fifo_queue.put(self.node)
        nodes_by_depth = []
        while not fifo_queue.empty():
            current_node: Dict[str, Trie] = fifo_queue.get()
            node_values = []
            for value, trie in current_node.items():
                node_values.append(value)
                fifo_queue.put(trie.node)
            nodes_by_depth.append(node_values)
        logger.info(nodes_by_depth)

    def __contains__(self, url: List[str]) -> bool:
        """
        Search if an url is in the Trie
        """
        url = self._split_url(url)
        if len(url) == 0:
            return self.end_token in self.node

        if url[0] not in self.node:
            return False

        else:
            return url[1:] in self.node[url[0]]

    def __repr__(self) -> str:
        return f"Trie({self.node})"


class UrlCache:
    CACHE_FACTORY = {"map": set, "trie": Trie}

    def __init__(self, dir: str, cache_class: str = "map") -> None:
        self.io = Path(dir, "cache.bin")
        self._cache: Union[set, Trie] = self.CACHE_FACTORY[cache_class]()

    def add(self, url: str) -> None:
        """
        Add an url to current cache
        """
        self._cache.add(url)

    def __contains__(self, url: str) -> bool:
        """
        Check if an url is in the cache
        """
        return url in self._cache

    def save(self) -> None:
        """
        Save the cache to the disk
        """
        with self.io.open("wb") as fb:
            pkl.dump(self._cache, fb)

    def load(self) -> None:
        """
        Load the cache into memory
        """
        with self.io.open("rb") as fb:
            self._cache = pkl.load(fb)

    def remove(self) -> None:
        """
        Remove the cache from disk
        """
        if self.io.exists():
            self.io.unlink()

    def getsize(self):
        """
        Get the size of the cache in memory in MB
        """
        from pympler import asizeof

        return asizeof.asizeof(self._cache) / (1024**2)

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}:\n\tClass: {self._cache.__class__.__name__},\n\tcache:{self._cache})"
