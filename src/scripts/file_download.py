# junyuan.wang  randomly download some files from the data.link to check the properties of the data

import argparse
import os
import random
import numpy as np
import pandas as pd
from omegaconf import OmegaConf
import subprocess
from multiprocessing import Pool
from pathlib import Path
import time


def get_data(cfg):
    file_path = cfg.input
    output_dir = cfg.output
    current_website = None
    with open(file_path, "r") as f:
        lines = f.readlines()

    pool = Pool(processes=8)  # You can adjust the number of processes as needed

    for line in lines:
        if line.startswith("@"):
            current_website = line[1:].strip()
            continue
        try:
            line_list = line.split("\t", 1)
            ext = line_list[0]
            url = line_list[1].strip()

        except Exception as e:
            print(f"Error parsing line {line}: {e}")
            print(f"ext: {ext}; url: {url}")
        pool.apply_async(download_file, args=(url, current_website, ext, output_dir))

    pool.close()
    pool.join()
    print(f"Finished downloading files from {file_path}")


def download_file(url, parent_folder, ext, base_dir, max_retries=3):
    save_dir = Path(base_dir) / parent_folder / ext
    save_dir.mkdir(parents=True, exist_ok=True)

    for retry in range(max_retries):
        try:
            subprocess.run(
                ["wget", "-q", "-P", str(save_dir), url], check=True, timeout=30
            )
            print(f"Downloaded {url} to {save_dir}")
            break  # Exit the loop if the download is successful
        except subprocess.TimeoutExpired as e:
            print(f"Timeout downloading {url}")
            if retry < max_retries - 1:
                print(f"Retrying ({retry + 1}/{max_retries}) in 5 seconds...")
                time.sleep(5)
        except subprocess.CalledProcessError as e:
            print(f"Error downloading {url}")
            if retry < max_retries - 1:
                print(f"Retrying ({retry + 1}/{max_retries}) in 5 seconds...")
                time.sleep(5)  # Wait for 5 seconds before retrying
            else:
                print(f"Maximum retries ({max_retries}) reached. Skipping {url}")
    time.sleep(1)


def main():
    parser = argparse.ArgumentParser(
        description="Download some files from the data.link to check the properties of the data"
    )

    parser.add_argument(
        "--input",
        "-i",
        type=str,
        help="Path to the sampled url file",
    )

    parser.add_argument(
        "--output",
        "-o",
        type=str,
        help="Path to the output file",
    )

    args = parser.parse_args()
    cfg = OmegaConf.create(vars(args))

    get_data(cfg)


main()
