import time
from argparse import ArgumentParser, Namespace
from dataclasses import dataclass
from pathlib import Path
from typing import List, Literal, Union
from urllib.parse import urlparse

import requests
from omegaconf import OmegaConf
from scrapy.crawler import CrawlerProcess, CrawlerRunner
from scrapy.settings import Settings
from scrapy.utils.log import configure_logging
from scrapy.utils.project import get_project_settings
from twisted.internet import defer, reactor

from crawler.cache import UrlCache
from crawler.spiders.stat import FocusCrawler, GenericCrawler
from utils.log import get_log

logger = get_log(__file__)

MULTIPROCESS_SETTINGS = {
    "DEPTH_PRIORITY": 1,
    "SCHEDULER_DISK_QUEUE": "scrapy.squeues.PickleFifoDiskQueue",
    "SCHEDULER_PRIORITY_QUEUE": "scrapy.pqueues.DownloaderAwarePriorityQueue",
    "SCHEDULER_MEMORY_QUEUE": "scrapy.squeues.FifoMemoryQueue",
    "REACTOR_THREADPOOL_MAXSIZE": 20,
}


@dataclass
class WebsiteMetadata:
    url: str
    domain: str
    should_be_prefixed: bool = False


def _get_safe_scrappy_config() -> Settings:
    """
    Load a scrapy config if found in the python path, else raise an error.
    """
    settings = get_project_settings()
    configure_logging(settings)

    try:
        assert (
            settings.attributes["BOT_NAME"].value == "statcheck"
        ), "BOT NAME should be statcheck"
    except AssertionError as err:
        logger.error(
            "Scrapy settings not found. Verify that this script is launch from src/"
        )
        raise err

    return settings


def _get_crawler(
    crawler_type: Literal["generic", "focus"]
) -> Union[GenericCrawler, FocusCrawler]:
    """
    Factory function to get the targeted crawler class
    """
    if crawler_type == "generic":
        return GenericCrawler

    elif crawler_type == "focus":
        return FocusCrawler

    else:
        raise ValueError(
            "Unrecognized crawler type. Should be either 'generic' or 'focus'"
        )


@defer.inlineCallbacks
def _crawl(
    sites_metadata: List[WebsiteMetadata],
    runner: CrawlerRunner,
    crawler_class: Union[GenericCrawler, FocusCrawler],
    out_dir: Path,
    max_page: int,
    dev: bool,
) -> None:
    """
    Deferred function to run crawler process sequentially.
    """
    for site in sites_metadata:
        yield runner.crawl(
            crawler_class,
            url=site.url,
            domain=site.domain,
            out_dir=out_dir,
            max_page=max_page,
            dev=dev,
        )
    reactor.stop()


def sequential_crawler_process(
    sites_metadata: List[WebsiteMetadata],
    settings: Settings,
    crawler_class: Union[GenericCrawler, FocusCrawler],
    out_dir: Path,
    max_page: int,
    dev: bool,
) -> None:
    """
    Run a sequential crawler process.
    """
    start = time.time()
    runner = CrawlerRunner(settings)
    _crawl(sites_metadata, runner, crawler_class, out_dir, max_page, dev)
    reactor.run()
    logger.info(
        f"{len(sites_metadata)} sites crawled, overall took {time.time()-start:.2f} s"
    )


def crawler_process(
    site_metadata: WebsiteMetadata,
    out_dir: Path,
    settings: Settings,
    crawler_class: Union[GenericCrawler, FocusCrawler],
    max_page: int,
    dev: bool,
) -> None:
    """
    Run a single crawler process
    """
    start = time.time()
    process = CrawlerProcess(settings)

    url_cache = UrlCache(dir=out_dir, cache_class="trie")
    url_cache.remove()

    process.crawl(
        crawler_class,
        url=site_metadata.url,
        domain=site_metadata.domain,
        out_dir=out_dir,
        max_page=max_page,
        dev=dev,
        cache=url_cache,
    )
    process.start()
    logger.info(f"Crawling {site_metadata.url} took {time.time()-start:.2f} s")
    url_cache.save()


def crawler_multiprocess(
    sites_metadata: List[WebsiteMetadata],
    out_dir: Path,
    settings: Settings,
    crawler_class: Union[GenericCrawler, FocusCrawler],
    max_page: int,
    dev: bool,
) -> None:
    """
    Run a single crawler process
    """
    start = time.time()

    seen_domains = set()
    for metadata in sites_metadata:
        if metadata.domain not in seen_domains:
            seen_domains.add(metadata.domain)
        else:
            metadata.should_be_prefixed = True

    for setting_name, value in MULTIPROCESS_SETTINGS.items():
        settings.set(setting_name, value)

    url_cache = UrlCache(dir=out_dir, cache_class="trie")
    url_cache.remove()
    process = CrawlerProcess(settings)
    for site_metadata in sites_metadata:
        process.crawl(
            crawler_class,
            url=site_metadata.url,
            domain=site_metadata.domain,
            should_be_prefixed=site_metadata.should_be_prefixed,
            out_dir=out_dir,
            max_page=max_page,
            dev=dev,
            cache=url_cache,
        )

    process.start()
    logger.info(
        f"{len(sites_metadata)} sites in parallel crawled, overall took {time.time()-start:.2f} s"
    )
    url_cache.save()


def parse_urls(file_or_url: Union[Path, str]) -> List[WebsiteMetadata]:
    """
    Parse a file containing url. Also return allowed domain for each url.
    """
    if isinstance(file_or_url, Path):
        with Path(file_or_url).open("r") as f:
            lines = f.read().splitlines()
        return [WebsiteMetadata(url=url, domain=urlparse(url).netloc) for url in lines]

    elif isinstance(file_or_url, str):
        return [WebsiteMetadata(url=file_or_url, domain=urlparse(file_or_url).netloc)]


def main(cfg: OmegaConf):
    """
    Main crawler function
    """

    out_dir = Path(cfg.out_dir)
    out_dir.mkdir(exist_ok=True, parents=True)

    if cfg.file:
        sites = parse_urls(Path(cfg.file))

    elif cfg.url:
        sites = parse_urls(cfg.url)

    else:
        raise RuntimeError(
            "You should supply at least on of the arguments --file or --url"
        )

    if len(sites) == 1:
        logger.info(
            f"Running sequential crawler from {cfg.file if cfg.file else cfg.url}"
        )
        crawler_process(
            *sites,
            out_dir=out_dir,
            settings=_get_safe_scrappy_config(),
            crawler_class=_get_crawler(cfg.crawler_type),
            max_page=cfg.max_page,
            dev=cfg.dev,
        )
    else:
        if cfg.parallel:
            if cfg.url:
                raise ValueError(
                    "Parameter --url can't be used with multiprocess crawler. Please use --file instead."
                )
            logger.info(f"Running multiprocess crawler from {cfg.file}")
            crawler_multiprocess(
                sites,
                out_dir=out_dir,
                settings=_get_safe_scrappy_config(),
                crawler_class=_get_crawler(cfg.crawler_type),
                max_page=cfg.max_page,
                dev=cfg.dev,
            )

        else:
            logger.info(f"Running sequential crawler from {cfg.file}")
            sequential_crawler_process(
                sites_metadata=sites,
                settings=_get_safe_scrappy_config(),
                crawler_class=_get_crawler(cfg.crawler_type),
                out_dir=out_dir,
                max_page=cfg.max_page,
                dev=cfg.dev,
            )


def set_cfg(args: Namespace):
    """
    Set the configuration object
    """
    cfg = OmegaConf.create(vars(args))
    excludes = ("file", "url")
    if all([getattr(cfg, exclude_arg) for exclude_arg in excludes]):
        raise RuntimeError("Cannot supply both arguments --file and --url")

    return cfg


if __name__ == "__main__":
    parser = ArgumentParser(description="Run a single/multiple crawler process.")

    parser.add_argument(
        "--out-dir",
        "-o",
        required=False,
        type=str,
        help="The out directory, where crawled pages/items will be write.",
    )

    parser.add_argument(
        "--crawler-type",
        "-ct",
        choices=["generic", "focus"],
        default="generic",
        type=str,
        help="The type of crawler to be used",
    )

    parser.add_argument(
        "--file",
        "-f",
        type=str,
        help="The absolute path to a file containing all the links to be crawl.",
    )

    parser.add_argument(
        "--url",
        "-u",
        type=str,
        help="An url to be crawl (should be used for development).",
    )

    parser.add_argument(
        "--max-page",
        "-mp",
        type=int,
        default=-1,
        help="The max number of page to be visited (should be used for development). -1 means to visit all the pages.",
    )
    parser.add_argument(
        "--dev",
        action="store_true",
        default=False,
        help="Whether to run the crawler in dev mode.",
    )

    parser.add_argument(
        "--parallel",
        action="store_true",
        default=False,
        help="Whether to run the crawler in parallel.",
    )

    args = parser.parse_args()
    cfg = set_cfg(args)
    main(cfg)
