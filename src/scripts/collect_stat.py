import csv
import datetime as dt
import logging
import sys
from argparse import ArgumentParser
from collections import Counter, defaultdict
from logging import getLogger
from pathlib import Path
from typing import Dict, List, Tuple, Union
from urllib.parse import urlparse, urlunparse

import pandas as pd
import tqdm
from omegaconf import OmegaConf

from crawler.spiders.stat import FILE_EXTENSIONS, EXCLUDED_KEYWORDS

LEVEL = logging.INFO

logging.basicConfig(
    stream=sys.stdout,
    level=LEVEL,
    format="[%(levelname)s] - %(asctime)s - %(name)s: %(message)s",
)
logger = logging.getLogger("analysis")

HOURS_TO_SEC_SCALE = 60 * 60


def get_total_crawling_time(file: Path) -> Tuple[float, bool]:
    """
    Get the total crawling time for a specific website
    """
    if not file.exists():
        return 0.0, False

    with file.open() as f:
        reader = csv.reader(f, delimiter="\t")
        start, end = [dt.datetime.fromisoformat(value) for (key, value) in reader]

    total_sec = (end - start).total_seconds()

    return total_sec, total_sec >= 10 * HOURS_TO_SEC_SCALE


def get_number_of_out_of_domains(file: Path) -> int:
    """
    Get the distincts number of out of domains websites found.
    """

    if not file.exists():
        return 0

    seen_domains = set()
    with file.open() as f:
        for link in f:
            domain = urlparse(link).netloc
            if domain not in seen_domains:
                seen_domains.add(domain)

    return len(seen_domains)


def get_data_distribution(dir: Path) -> Dict[str, Union[str, int]]:
    """
    Get the number of datasets per format.
    """
    file_datasets = dir.joinpath("data.link")
    html_datasets = dir.joinpath("html.link")

    extensions = set(FILE_EXTENSIONS)
    extension_count = defaultdict(int)
    sampled_urls = defaultdict(list)
    sample_size = 5

    if file_datasets.exists():
        df = pd.read_csv(file_datasets, sep="\t", header=None)
        df = df.drop_duplicates()
        # drop excluded keywords
        for keyword in EXCLUDED_KEYWORDS:
            df = df[~df[0].str.contains(keyword)]
        # Count occurrences of each extension using vectorized operations
        for ext in extensions:
            ext_pattern = f"\\.{ext}\\b"
            ext_mask = df[0].str.contains(ext_pattern)
            ext_df = df[ext_mask]
            extension_count[ext] = len(ext_df)
            if len(ext_df) > 0:
                sampled_urls[ext] = ext_df.sample(min(sample_size, len(ext_df)))[0].tolist()

        

    if html_datasets.exists():
        df = pd.read_csv(html_datasets, sep="\t", header=None)
        df = df.drop_duplicates()
        extension_count["html"] += len(df)

    extension_count["total"] = sum(extension_count.values())
    extension_count["website"] = dir.name
    return extension_count, sampled_urls


def get_n_visited_pages(file: Path, chunk_size=1000000) -> Tuple[int, bool]:
    """
    Get the total number of visited pages.
    """
    tot = 0

    if not file.exists():
        return tot, True

    for chunk in pd.read_csv(file, chunksize=chunk_size, delimiter="\t"):
        tot += chunk.size
        # for idx, row in chunk.iterrows():
        #     from_, url = row

    return int(tot), False


def main(cfg) -> None:
    """
    Collect statistics for each websites crawled. The statistics are write into two files.
        - general_statistics.csv : Contains general statistics on all the websites
        - data_distribution.csv : Contains the data distribution of the websites
    """
    crawler_dir = Path(cfg.dir)
    websites_statistics, data_distribution = [], []
    for website_dir in [path for path in crawler_dir.iterdir() if path.is_dir()]:
        logger.info(f"Collecting stats for {website_dir}...")
        total_time, has_reach_TO = get_total_crawling_time(
            website_dir.joinpath("stat.tsv")
        )
        n_ood = get_number_of_out_of_domains(website_dir.joinpath("out_of_domain.link"))
        n_visited, is_empty = get_n_visited_pages(
            website_dir.joinpath("visited.link"), chunk_size=cfg.chunk_size
        )

        with open(crawler_dir.joinpath("sample_urls.csv"), "a") as f:
            f.write(f"@{website_dir.name}\n")

            data_distrib, sampled_urls = get_data_distribution(website_dir)
            data_distribution.append(data_distrib)
            for ext, urls in sampled_urls.items():
                for url in urls:
                    f.write(f"{ext}\t{url}\n")

        start_url = ""
        with open(website_dir.joinpath("visited.link"), "r") as f:
            f.readline()
            line = f.readline()
            if line:
                start_url = line.split()[0]
                logger.info(f"Start URL: {start_url}")

        websites_statistics.append(
            {
                "website": website_dir.name,
                "crawling_time_sec": total_time,
                "n_visited": n_visited,
                "throughput_page_second": n_visited / total_time if total_time else 0.0,
                "uncrawlable": is_empty,
                "has_reach_TO": has_reach_TO,
                "n_out_of_domains": n_ood,
                "data_volume": data_distrib["total"],
                "start_url": start_url,
            }
        )

    pd.DataFrame(data_distribution).fillna(0.0).to_csv(
        crawler_dir.joinpath("data_distribution.csv"), index=False
    )

    pd.DataFrame(websites_statistics).to_csv(
        crawler_dir.joinpath("general_statistics.csv"), index=False
    )


if __name__ == "__main__":
    parser = ArgumentParser(description="Run a single/multiple crawler process.")

    parser.add_argument(
        "--dir",
        "-d",
        required=False,
        type=str,
        help="The out directory, where crawled pages/items have been wrote.",
    )

    parser.add_argument(
        "--chunk_size",
        "-cs",
        type=int,
        default=1000000,
        help="The chunk size to used to read the visited.link file",
    )

    args = parser.parse_args()
    cfg = OmegaConf.create(vars(args))

    main(cfg)
